#include <iostream>
#include <vector>
#include <map>
#include <algorithm>

#define maxN 50001
#define maxW 10000001

using namespace std;


int findWeight( int u, int v);
void Prims();
int minKey( int key[], bool mstSet[]);
void twoGroup(int v1, int v2);
bool findadj( int u, int v);
void DFS(int i);
bool isMinInCut(int wt);

int Prims_again();

int mst_weight;
int T, n, m, a, b, c;
vector<int> topoint[maxN];
vector<int> toweight[maxN];
vector<int> MSTvec[maxN];
vector<int> set1;
vector<int> set2;
map<pair<int,int>, int> MST;


static int parent[maxN];
static int key[maxN];
static bool mstSet[maxN];
static bool visit[maxN];

int main ()
{
	int v1, v2, ans_cnt, ans_wt, now_wt;
	pair<int,int> temp_pair;
	map<pair<int,int>, int>::iterator mapit;
	vector<int>::iterator toptit;
	vector<int>::iterator towtit;

	cin >> T;
	for( int ts = 0; ts < T; ts++)
	{
		ans_cnt = 0;
		ans_wt = 0;
		mst_weight = 0;
		MST.clear();

		for( int vs = 1; vs <= n; vs++)
		{
			topoint[vs].clear();
			toweight[vs].clear();
			MSTvec[vs].clear();
		}

		cin >> n >> m;
		
		for( int es = 0; es < m; es++)
		{
			cin >> a >> b >> c;
			topoint[b].push_back(a);
			toweight[b].push_back(c);
			topoint[a].push_back(b);
			toweight[a].push_back(c);
		}

		///find a certain MST
		Prims();
		
		mapit = MST.begin();
		///test MUST
		while(mapit != MST.end())
		{
			temp_pair = mapit->first;
			v1 = temp_pair.first;
			v2 = temp_pair.second;
			now_wt = mapit->second;
			
			toptit = MSTvec[v1].begin();
			while ( toptit != MSTvec[v1].end() )
			{
				if ( *toptit == v2 )
				{
					MSTvec[v1].erase(toptit);
					break;
				}
				toptit++;
			}

			toptit = MSTvec[v2].begin();
			while ( toptit != MSTvec[v2].end() )
			{
				if ( *toptit == v1 )
				{
					MSTvec[v2].erase(toptit);
					break;
				}
				toptit++;
			}


			twoGroup(v1, v2);

			if ( isMinInCut(now_wt) )
			{
				ans_cnt++;
				ans_wt += now_wt;
			}

			MSTvec[v1].push_back(v2);
			MSTvec[v2].push_back(v1);


			set1.clear();
			set2.clear();
			mapit++;
		}

		cout << ans_cnt << " " << ans_wt << endl;
	}

	//system("pause");
	return 0;

}



void Prims()
{
	int count, temp, u;
	bool isFound;

	for ( int i = 0; i <= n; i++)
	{
		key[i] = maxW;
		mstSet[i] = false;
		parent[i] = -2;
	}

	key[1] = 0;
	parent[1] = -1;

	for ( count = 0; count < (n - 1); count++)
	{
		u = minKey( key, mstSet);
		mstSet[u] = true;
		for( int v = 1; v <= n; v++)
		{
			isFound = false;
			temp = findWeight( u, v);

			if ( temp != maxW )
				isFound = true;
			
			if ( isFound && mstSet[v] == false && temp < key[v] )
			{
				parent[v] = u;
				key[v] = temp;
			}
		}
	}

	//return tree;
	for(int j = 2; j <= n; j++)
	{
		temp = findWeight(j, parent[j]);
		MSTvec[j].push_back(parent[j]);
		MSTvec[parent[j]].push_back(j);
		MST.insert( pair< pair< int, int>, int>( pair< int, int>( parent[j], j), temp));
		mst_weight += temp;
	}
}

int minKey( int key[], bool mstSet[])
{
	int min = maxW;
	int min_index = 0;

	for ( int v = 1; v <= n; v++)
		if ( (mstSet[v] == false) && (key[v] < min) )
		{
			min = key[v];
			min_index = v;
		}

	return min_index;
}


int findWeight( int u, int v)
{
	int min_weight = maxW;
	int nowkey = 0, temp;
	vector<int>::iterator it;
	it = topoint[u].begin();
	while ( it != topoint[u].end())
	{
		if ( *it == v )
		{
			temp = toweight[u][nowkey];
			if ( min_weight > temp )
				min_weight = temp;
		}
		it++, nowkey++;
	}

	return min_weight;
}



bool findadj( int u, int v)
{
	vector<int>::iterator it = find( MSTvec[u].begin(), MSTvec[u].end(), v);
	if ( it != MSTvec[u].end() )
		return true;
	else
		return false;
}



void twoGroup(int v1, int v2)
{
	for ( int es = 1; es <= n; es++)
		visit[es] = false;

	DFS(v1);
	for( int i = 1; i <= n; i++)
	{
		if (visit[i])
			set1.push_back(i);
		else
			set2.push_back(i);
	}
}

void DFS(int i)
{
	for( int j = 1; j <= n; j++)
	{
		if( findadj( i, j) && !visit[j] )
		{
			visit[j] = true;
			DFS(j);
		}
	}
}


bool isMinInCut(int wt)
{
	vector<int>::iterator s1it;
	vector<int>::iterator s2it;
	bool output = true;
	int sameweight = 0;
	int temp;
	
	for( s1it = set1.begin(); s1it != set1.end(); s1it++)
	{
		for( s2it = set2.begin(); s2it != set2.end(); s2it++)
		{
			temp = findWeight( *s1it, *s2it);

			if( temp < wt )
			{
				output = false;
				break;
			}
			else if ( temp == wt )
			{
				sameweight++;
				if (sameweight == 2)
				{
					output = false;
					break;
				}
			}
		}
	}
	return output;
}