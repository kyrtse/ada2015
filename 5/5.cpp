#include <iostream>

using namespace std;
int rabbit_house( int input_c, int* input_array, int input_len);
void print_rabbit( int* input_array, int input_len);


int main()
{
	int length, T, n, c;
	int a = 5;

	cin >> T;

	for( int i = 0; i < T; i++)
	{
		cin >> n;
		length = 0;
		int rabbit[2000] = {0};
		for( int j = 0; j < n; j++)
		{
			cin >> c;
			length = rabbit_house( c, &rabbit[0], length);
			print_rabbit( &rabbit[0], length);
			if( j < (n - 1) )
				cout << " ";
		}
		cout << endl;
	}

	//system("pause");
	return 0;
}


int rabbit_house( int input_c, int* input_array, int input_len)
{
	int sum_tmp = input_c;
	int len = input_len;
	int rabbit[2000] = {0};
	if ( len == 0 )
	{
		while( sum_tmp > 9 )
		{
			rabbit[len] = 9;
			len++;
			sum_tmp -= 9;
		}
		rabbit[len] = sum_tmp;
		len++;
	}
	else
	{
		int k = len;
		while( (sum_tmp > 0) && (k > 0) )
		{
			k--;
			rabbit[k] = input_array[k];
			sum_tmp -= input_array[k];
		}

		if ( (k == 0) && ((rabbit[k] + sum_tmp) > 9) )
		{
			int d;
			while ( (rabbit[k] + sum_tmp) > 9 )
			{
				d = 9 - rabbit[k];
				rabbit[k] = 9;
				sum_tmp -= d;
				k++;
				if ( k == len )
					len++;
			}
			rabbit[k] += sum_tmp;
			sum_tmp = 0;
		}
		else if ( (k == 0) && (sum_tmp > 0) )
		{
			rabbit[k] += sum_tmp;
			sum_tmp = 0;
		}
		else if ( (sum_tmp <= 0) && (k != (len - 1)) )
		{
			sum_tmp += rabbit[k];
			rabbit[k] = 0;
			k++;
			while ( (rabbit[k] <= input_array[k]) )
			{
				while( rabbit[k] == 9 )
				{
					if ( k == (len - 1) )
						len++;
					rabbit[k] = 0;
					sum_tmp += 9;
					k++;
				}
				rabbit[k]++;
				sum_tmp--;

				int j = 0;
				while( sum_tmp > 9 )
				{
					rabbit[j] = 9;
					sum_tmp -= 9;
					j++;
				}
				rabbit[j] = sum_tmp;
			}
		}
		else if ( (sum_tmp <= 0) && (k == (len - 1)) )
		{
			len++;
			sum_tmp = input_c;
			for( int i = 0; i < len; i++)
			{
				if ( sum_tmp > 9 )
				{
					rabbit[i] = 9;
					sum_tmp -= 9;
				}
				else if ( sum_tmp > 1 )
				{
					rabbit[i] = sum_tmp - 1;
					sum_tmp -= sum_tmp - 1;
				}
				else if ( i != (len - 1) )
					rabbit[i] = 0;
				else
					rabbit[i] = 1;
			}
		}
	}
	for( int i = 0; i < len; i++)
	{
		input_array[i] = rabbit[i];
	}
	return len;
}

void print_rabbit( int* input_array, int input_len)
{
	for( int i = input_len - 1; i >= 0; i--)
	{
		cout << input_array[i];
	}
}
