#include <stdio.h>
#include <iostream>

using namespace std;

int main(){
	int T,n;
	int a,b,x;
	cin >> T;
	for( n=T; n>0; n--){
		cin >> x;
		a = x/2;
		b = x - a;
		cout << a << " " << b << endl;
	}
	return 0;
}