#include <iostream>
#include "stdint.h"

//#include <algorithm>
//#include <vector>

using std::cin;
using std::cout;
using std::endl;

void divide( int arr[], int64_t head, int64_t tail, int64_t num, int64_t c, uint64_t e, int64_t p);
//void combine( int arr[], int64_t head, int64_t mid, int64_t tail, int64_t n1, int64_t n2, int64_t c, uint64_t e, int64_t p);
void merge( int arr[], int64_t head, int64_t mid, int64_t tail, int64_t c, uint64_t e, int64_t p);
//void insert( int arr[], int64_t pivot, int64_t tail, int64_t c, uint64_t e, int64_t p);
bool fighting( int64_t i, int64_t j, int64_t c, uint64_t e, int64_t p);
uint64_t AEP( int64_t a, uint64_t expo, int64_t p);

int64_t table[400001];
uint64_t tabletemp[400001];
bool earr[70];
uint64_t exlength;
float f;

//void testAEP()
//{
//	uint64_t a = (26*26*26*26) % 17;
//	a *= (( 26*26*26 ) % 17);
//	a *= (( 26*26*26 ) % 17);
//	a *= (( 26*26*26 ) % 17);
//	a = a%17;
//
//	
//
//	cout << AEP( 26, 13, 17) << endl;
//	cout << a << endl;
//
//	cout << AEP( 5, 3, 7) << endl;
//	cout << (5*5*5)%7 << endl;
//
//	cout << AEP( 2, 6, 17) << endl;
//}
//
//void testfighting()
//{
//
//	cout << fighting( 2, 1, 5, 3, 37) << endl;
//	cout << fighting( 1, 3, 5, 3, 37) << endl;
//	cout << fighting( 3, 4, 5, 3, 37) << endl;
//	cout << fighting( 4, 6, 5, 3, 37) << endl;
//	cout << fighting( 6, 5, 5, 3, 37) << endl;
//	cout << fighting( 5, 7, 5, 3, 37) << endl;
//	cout << fighting( 7, 8, 5, 3, 37) << endl;
//
//	cout << endl;
//
//	
//	cout << fighting( 6, 5, 5, 3, 37) << endl;
//}


int main()
{
	int64_t T, n, c, p;
	uint64_t e;
	int64_t i;
	int order[200001];

	//testfighting();

	cin >> T;

	for( int iteration = 0; iteration < T; iteration++)
	{
		
		cin >> n;
		cin >> c;
		cin >> e;
		cin >> p;

		/*==================table initialize========================*/
		for( i = 0; i <= n; i++)
		{
			table[i] = -1;
			tabletemp[i] = p;
			order[i] = i;
		}
		for( i = n + 1; i < 400001; i++)
		{
			table[i] = -1;
			tabletemp[i] = p;
		}
		/*===================================================================*/
		f = p/2;
		/*========================compute exponential========================*/
		bool odd;

		exlength = 0;
		for( i = 0; e > 1; i++, exlength++)
		{
			odd = (e & 1) ? true : false;
			earr[i] = odd;
			e >>= 1;
		}

		/*===================================================================*/


		divide( order, 1, n, n, c, e, p);
		
		for( i = 1; i <= n; i++)
			printf("%d ", order[i]);
			//cout << order[i] << " ";
		//cout << endl;
		printf("\n");
	}

	system("pause");
	
	return 0;

}

void divide( int arr[], int64_t head, int64_t tail, int64_t num, int64_t c, uint64_t e, int64_t p)
{
	if ( num == 2 )
	{
		if ( !fighting( arr[head], arr[tail], c, e, p) )
		{
			arr[head] = arr[head] + arr[tail];
			arr[tail] = arr[head] - arr[tail];
			arr[head] = arr[head] - arr[tail];
		}
	}
	else if( num > 2 )
	{
		int64_t mid = (head + tail) / 2;
		int64_t n1 = mid - head + 1;
		int64_t n2 = tail - mid;
		divide( arr, head, mid, n1, c, e, p);
		divide( arr, (mid + 1), tail, n2, c, e, p);
		//combine( arr, head, mid, tail, n1, n2, c, e, p);
		merge( arr, head, mid, tail, c, e, p);
	}
}


//void combine( int arr[], int64_t head, int64_t mid, int64_t tail, int64_t n1, int64_t n2, int64_t c, uint64_t e, int64_t p)
//{
//	int64_t i;
//	for( i = mid; i >= head; i--)
//		insert( arr, i, tail, c, e, p);
//}


void merge( int arr[], int64_t head, int64_t mid, int64_t tail, int64_t c, uint64_t e, int64_t p)
{
	int mgarr[200001];
	int64_t mptr = 0;
	int64_t ptr1 = head;
	int64_t ptr2 = mid + 1;
	int64_t it;
	while( (ptr1 <= mid) && (ptr2 <= tail))
	{
		if( fighting( arr[ptr1], arr[ptr2], c, e, p) )
		{
			mgarr[mptr] = arr[ptr1];
			ptr1++;
		}
		else
		{
			mgarr[mptr] = arr[ptr2];
			ptr2++;
		}
		mptr++;
	}

	while( ptr1 <= mid )
	{
		mgarr[mptr] = arr[ptr1];
		ptr1++;
		mptr++;
	}

	while( ptr2 <= tail )
	{
		mgarr[mptr] = arr[ptr2];
		ptr2++;
		mptr++;
	}

	for( it = 0; it < mptr; it++)
		arr[head + it] = mgarr[it];
}

//void insert( int arr[], int64_t pivot, int64_t tail, int64_t c, uint64_t e, int64_t p)
//{
//	// insert the tail of the first half into the last half
//	int64_t k , temp;
//
//	temp = arr[pivot];
//	for( k = (pivot + 1); !fighting( temp, arr[k], c, e, p) && (k <= tail); k++)
//		arr[k - 1] = arr[k];
//	arr[k - 1] = temp;
//}


bool fighting( int64_t i, int64_t j, int64_t c, uint64_t e, int64_t p)
{
	int64_t temp;
	uint64_t temp2;
	int64_t index;

	index = i - j + 200000;

	if( tabletemp[index] == p )
	{
		//avoid negtive (i - j)
		temp = (i - j) % p;
		while ( temp < 0 )
			temp += p;
		temp %= p;
	
		//multiply c
		temp2 = (c * temp) % p; 
		tabletemp[index] = temp2;
	}
	else
		temp2 = tabletemp[index];

	//multiply all terms
	temp2 = temp2 * AEP( (i + j), e, p);
	temp2 %= p;

	//judge
	if ( temp2 > f )
		return true;
	else
		return false;
}
	



uint64_t AEP( int64_t a, uint64_t expo, int64_t p)
{
	//This function is to return (a^e) mod p
	if( expo == 0)
		return 1;
	else if( table[a] == -1 )
	{
		//Judge if a >= p
			a %= p;
	
		//Fermat Little Theorm
			expo %= (p - 1);
	
		/*=============================================*/

		////decompose exponetial
		//vector<bool> ivec;
		//bool odd;
		//for( int i=0; expo > 1; i++)
		//{
		//	odd = (expo & 1) ? true : false;	
		//	ivec.push_back(odd);
		//	expo >>= 1;
		//}
		//reverse( ivec.begin(), ivec.end());

		////compute the multiplication
		//uint64_t temp;
		//uint64_t result = a;
		//for( vector<bool>::iterator it = ivec.begin(); it != ivec.end(); it++)
		//{
		//	temp = (*it & 1) ? a : 1;
		//	temp %= p;
		//	result = (result * result) % p;
		//	result = (temp * result) % p;
		//}

		/*=============================================*/

		int64_t i; 
		uint64_t temp;
		uint64_t result = a;
		for( i = exlength - 1; i >= 0; i--)
		{
			temp = (earr[i] & 1) ? a : 1;
			temp %= p;
			result = (result * result) % p;
			result = (temp * result) % p;
		}

		table[a] = result;
		return result;
	}
	else
	{
		return table[a];
	}
}