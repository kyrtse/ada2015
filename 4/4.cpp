#include <iostream>
#include "stdint.h"

//#include <ctime>


using std::cout;
using std::cin;
using std::endl;

void initialize_table( int64_t *iarr, int64_t digit);
void counting_add( int64_t *iarr, int64_t digit, int64_t head, int64_t mod7, int64_t g7, int64_t subs, int64_t addn);
int64_t index( int64_t digit, int64_t head, int64_t mod7, int64_t num7, int64_t n7_4);
int64_t trf7_4( int64_t num, int64_t digit);
void print_block( int64_t *iarr, int64_t digit, int64_t head);
void copy0( int64_t *iarr, int64_t digit);
void build_table( int64_t *iarr, int64_t digit);
int64_t exponent( int64_t m, int64_t exp);
int64_t digit_length( int64_t num);
int64_t largest_digit( int64_t num);
int64_t search_interval( int64_t *iarr, int64_t l, int64_t r);
int64_t search_num( int64_t *iarr, int64_t input);
int64_t search_lucky_seven( int64_t l, int64_t r);
bool testnum( int64_t originaln);

int main()
{
	//head, mod7, need # of 7, # of 7-4;
	int64_t T, l, r;

	const int64_t ds_size = 20 * 10 * 7 * 4 * 37;
	static int64_t ds[20 * 10 * 7 * 4 * 37] = {0};

	//std::clock_t start;
	//double duration;
	//start = std::clock();


	initialize_table( ds, 1);
	build_table( ds, 2);
	build_table( ds, 3);
	build_table( ds, 4);
	build_table( ds, 5);
	build_table( ds, 6);
	build_table( ds, 7);
	build_table( ds, 8);
	build_table( ds, 9);
	build_table( ds, 10);
	build_table( ds, 11);
	build_table( ds, 12);
	build_table( ds, 13);
	build_table( ds, 14);
	build_table( ds, 15);
	build_table( ds, 16);
	build_table( ds, 17);
	build_table( ds, 18);
	build_table( ds, 19);

	//duration = ( std::clock() - start ) / (double) CLOCKS_PER_SEC;
	//cout << "time : " << duration << endl;

	//print_block( ds, 1, 0);
	//print_block( ds, 1, 1);
	//print_block( ds, 1, 2);
	//print_block( ds, 1, 3);
	//print_block( ds, 1, 4);
	//print_block( ds, 1, 5);
	//print_block( ds, 1, 6);
	//print_block( ds, 1, 7);
	//print_block( ds, 1, 8);
	//print_block( ds, 1, 9);

	
	//print_block( ds, 2, 0);
	//print_block( ds, 2, 1);
	//print_block( ds, 2, 2);
	//print_block( ds, 2, 3);
	//print_block( ds, 2, 4);
	//print_block( ds, 2, 5);
	//print_block( ds, 2, 6);
	//print_block( ds, 2, 7);
	//print_block( ds, 2, 8);
	//print_block( ds, 2, 9);

	//print_block( ds, 3, 6);
	//print_block( ds, 7, 6);
	//print_block( ds, 6, 3);
	//print_block( ds, 5, 6);
	//print_block( ds, 4, 3);
	//print_block( ds, 2, 3);
	//print_block( ds, 1, 7);
	//print_block( ds, 1, 6);
	//cout << search_num( ds, 7474047 ) << endl;
	//cout << search_num( ds, 777 ) << endl;
	//cout << ds[ index( 1, 7, 0, 1, trf7_4( -1, 1)) ] << endl;;
	cin >> T;
	for( int64_t it = 0; it < T; it++)
	{
		cin >> l;
		cin >> r;
		if( r < l )
			cout << "Error" << endl;
		cout << search_interval( ds, l, r) << endl;
		//cout << search_lucky_seven(l, r) << endl;
	}


	system("pause");
	return 0;
	

}

void initialize_table( int64_t *iarr, int64_t digit)
{
	int64_t head, mod7, g7, subs, it, jt;

	for( mod7 = 0; mod7 < 7; mod7++)
	{
		for( head = 0; head < 10; head++)
		{
			for( g7 = 3; g7 >= 0; g7--)
			{
				for( subs = digit; subs >= -digit; subs--)
				{
					if( (head % 7) == mod7 )
					{
						if( (head != 7) && (head != 4) && (subs == 0) && (g7 == 0) )
							counting_add( iarr, digit, head, mod7, g7, subs, 1);
						else if( (head == 4) && (subs == -1) && (g7 == 0) )
							counting_add( iarr, digit, head, mod7, g7, subs, 1);
						else if( (head == 7) && (subs == 1) && (g7 == 1) )
							counting_add( iarr, digit, head, mod7, g7, subs, 1);
					}
					if ( head != 9 )
						iarr[ index( digit, (head + 1), mod7, g7, trf7_4( subs, digit)) ] = iarr[ index( digit, head, mod7, g7, trf7_4( subs, digit)) ];
				} // subs
			} //g7
		} //head
	} //mod7	
}

void counting_add( int64_t *iarr, int64_t digit, int64_t head, int64_t mod7, int64_t g7, int64_t subs, int64_t addn)
{
	int64_t it, jt;
	for( it = g7; it >= 0; it--)
		for( jt = subs; jt >= -digit; jt--)
			iarr[ index( digit, head, mod7, it, trf7_4( jt, digit)) ] += addn;
}

void build_table( int64_t *iarr, int64_t digit)
{
	int64_t head, mod7, g7, subs, modhead, modtemp, addtemp, ti, tj;
	
	copy0( iarr, digit);
	
	for( head = 1; head < 10; head++)
	{
		modhead = (head * exponent( 10, digit)) % 7;
		for( mod7 = 0; mod7 < 7; mod7++)
		{
			for( g7 = 3; g7 >= 0; g7--)
			{
				for( subs = digit; subs >= -digit; subs--)	
				{
					modtemp = (7 + mod7 - modhead) % 7;
					if ( (head != 7) && (head != 4) )
					{
						addtemp = iarr[ index( digit, 0, modtemp, g7, trf7_4( subs, digit)) ];
						iarr[ index( digit, head, mod7, g7, trf7_4( subs, digit)) ] += addtemp;
					}
					else if ( head == 4 )
					{
						ti = subs + 1;
						if( ti > digit)
							ti = digit;
						addtemp = iarr[ index( digit, 0, modtemp, g7, trf7_4( ti, digit)) ];
						iarr[ index( digit, head, mod7, g7, trf7_4( subs, digit)) ] += addtemp;
					}
					else if ( head == 7 )
					{
						ti = subs - 1;
						tj = g7 - 1;
						if( ti < -digit)
							ti = -digit;
						if( tj < 0)
							tj = 0;
						addtemp = iarr[ index( digit, 0, modtemp, tj, trf7_4( ti, digit)) ];
						iarr[ index( digit, head, mod7, g7, trf7_4( subs, digit)) ] += addtemp;
					}
					if ( head != 9 )
						iarr[ index( digit, (head + 1), mod7, g7, trf7_4( subs, digit)) ] = iarr[ index( digit, head, mod7, g7, trf7_4( subs, digit)) ];
				}
			}
		}
	} //head

}

void copy0( int64_t *iarr, int64_t digit)
{
	int64_t it, mod7, g7, subs;
	it = digit - 1;
	for( mod7 = 0; mod7 < 7; mod7++)
	{
		for( g7 = 3; g7 >= 0; g7--)
		{
			for( subs = it; subs >= -it; subs--)
			{
				if( subs != -it )
				{
					iarr[ index( digit, 0, mod7, g7, trf7_4( subs, digit)) ] += iarr[ index( it, 9, mod7, g7, trf7_4( subs, it)) ];
					iarr[ index( digit, 1, mod7, g7, trf7_4( subs, digit)) ] = iarr[ index( digit, 0, mod7, g7, trf7_4( subs, digit)) ];
				}
				else
				{
					iarr[ index( digit, 0, mod7, g7, trf7_4( subs, digit)) ] += iarr[ index( it, 9, mod7, g7, trf7_4( subs, it)) ];
					iarr[ index( digit, 1, mod7, g7, trf7_4( subs, digit)) ] = iarr[ index( digit, 0, mod7, g7, trf7_4( subs, digit)) ];
					iarr[ index( digit, 0, mod7, g7, trf7_4( (subs - 1), digit)) ] = iarr[ index( digit, 0, mod7, g7, trf7_4( subs, digit)) ];
					iarr[ index( digit, 1, mod7, g7, trf7_4( (subs - 1), digit)) ] = iarr[ index( digit, 0, mod7, g7, trf7_4( subs, digit)) ];
				}
			}
		}
	}
}

int64_t index( int64_t digit, int64_t head, int64_t mod7, int64_t num7, int64_t n7_4)
{
	int64_t d2 = 10;
	int64_t d3 = 7;
	int64_t d4 = 4;
	int64_t d5 = 37;
	int64_t pt = digit * d2 * d3 * d4 * d5 + head * d3 * d4 * d5 + mod7 * d4 * d5 + num7 * d5 + n7_4;
	return pt;
}

int64_t trf7_4( int64_t num, int64_t digit)
{
	return (num + digit);
}

void print_block( int64_t *iarr, int64_t digit, int64_t head)
{
	cout << "(digit, head) = " << digit << "," << head << endl << endl;
	for( int64_t k = 0; k < 7; k++)
	{
		cout << "mod = " << k << endl;
		cout << "\t3\t2\t1\t0\t" << endl;
		cout << "\t=========================" << endl;
		for( int64_t i = digit; i >= -digit; i--)
		{
			cout << i << "\t";
			for( int64_t j = 3; j >= 0; j--)
				cout << iarr[ index( digit, head, k, j, trf7_4( i, digit)) ] << "\t";
			cout << endl;
		}
		cout << "\t=========================" << endl << endl;
	}
	cout << endl;
}

int64_t exponent( int64_t m, int64_t exp)
{
	int64_t result = 1;
	for( int64_t i = (exp - 1); i >= 1; i--)
		result *= m;
	return result;
}

int64_t search_interval( int64_t *iarr, int64_t l, int64_t r)
{
	return (search_num( iarr, r) - search_num( iarr, (l - 1)));
}

int64_t search_num( int64_t *iarr, int64_t input)
{
	//if ( input < 777 )
	//	return 0;
	//else if ( input < 7077 )
	//	return 1;
	//else if ( input < 7707 )
	//	return 2;
	//else if ( input < 7770 )
	//	return 3;
	//else if ( input < 7777 )
	//	return 4;
	//else if ( input == 7777 )
	//	return 5;
	//else
	{
		int64_t sum, residue, length, head, digit, fullhead, tempmod, need7, sub74, cnt7, cnt4, alen, blen, modplus;

		sum = 0;
		residue = input;
		length = digit_length(input);
		cnt7 = 0;
		cnt4 = 0;
		tempmod = 0;
		modplus = 0;
		need7 = 3;
		sub74 = 0;

		for( digit = length; digit >= 1; digit--)
		{
			head = largest_digit(residue);
			fullhead = head * exponent( 10, digit);
			
			//cout << residue << endl;
			//sub74
			sub74 = 1 - (cnt7 - cnt4);
			//if ( sub74 > (digit + 1) )
			//	sub74 = (digit + 1);
			//else if ( sub74 < -(digit + 1) )
			//	sub74 = -(digit + 1);

			if ( sub74 > (digit + 1) )
				sub74 = (digit + 1);
			else if ( sub74 < -digit )
				sub74 = -digit;

			//need7
			need7 = 3 - cnt7;
			if ( need7 < 0 )
				need7 = 0;

			// cnt 7 and 4
			if ( head == 7 )
				cnt7++;
			else if ( head == 4 )
				cnt4++;

			if ( digit != 1 )
			{
				if( head != 0 )
					sum += iarr[ index( digit, (head - 1), tempmod, need7, trf7_4( sub74, digit)) ];
			}
			else
			{
				sum += iarr[ index( digit, head, tempmod, need7, trf7_4( sub74, digit)) ];
			}

			//cout << digit <<","<<head<<","<<tempmod<<","<<need7<<","<<sub74 << endl;
			modplus = (modplus + (fullhead % 7)) % 7;
			tempmod = (7 - modplus) % 7;
			//tempmod = (tempmod + (7 - (fullhead % 7))) % 7;
			alen = digit_length(residue);
			residue = residue - fullhead;
			blen = digit_length(residue);
			while( blen < (alen - 1) )
			{
				digit--;
				alen--;
			}
			//cout << tempmod << endl;
		}
		return sum;
	}
}

int64_t largest_digit( int64_t num)
{
	int64_t result, tmp;
	result = 0;
	tmp = num;
	if( tmp == 0 )
		result = 0;
	else
	{
		while( tmp > 0 )
		{
			result = tmp;
			tmp /= 10;
		}
	}
	return result;
}

int64_t digit_length( int64_t num)
{
	int64_t result;

	result = 0;
	if( num == 0 )
		result = 1;
	while( num > 0 )
	{
		num = num / 10;
		result++;
	}
	return result;
}

int64_t search_lucky_seven( int64_t l, int64_t r)
{
	int64_t i, sum;
	sum = 0;
	for( i = l; i <= r; i++)
	{
		if( testnum(i) )
			sum++;
	}
	return sum;
}

bool testnum( int64_t originaln)
{
	int64_t length, digit, cnt7, cnt4, tmp, num;
	cnt7 = 0;
	cnt4 = 0;
	num = originaln;
	length = digit_length(num);
	for( digit = 0; digit < length; digit++)
	{
		tmp = largest_digit(num);
		if( tmp == 7)
			cnt7++;
		if( tmp == 4)
			cnt4++;
		num = num - tmp * exponent(10, digit_length(num));
	}
	if ( ((originaln % 7) == 0) && (cnt7 >= 3) && ((cnt7 - cnt4) > 0) )
		return true;
	else
		return false;
}